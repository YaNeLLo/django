from rest_framework.routers import DefaultRouter
from coursework.apps.users.views import UserViewSet

router = DefaultRouter()

router.register('user', UserViewSet)