from django.shortcuts import render
from .models import Level1,Level2,Level3,Level4,Level5
from django.urls import reverse

def index(request):
    return render(request, 'levels/index.html')

def level(request,level_id,sort_by=''):
    order='-id'
    if sort_by == 'score':
        order='-score'
    if sort_by == 'coins':
        order='-coins'
    if sort_by == 'time':
        order='time'
    if sort_by == 'date':
        order='-date'
    if level_id == 1:
        leaderboard = Level1.objects.order_by(order)
        return render(request, 'levels/leaderboard.html', {'leaderboard': leaderboard,'level_id':level_id})
    if level_id == 2:
        leaderboard = Level2.objects.order_by(order)
        return render(request, 'levels/leaderboard.html', {'leaderboard': leaderboard,'level_id':level_id})
    if level_id == 3:
        leaderboard = Level3.objects.order_by(order)
        return render(request, 'levels/leaderboard.html', {'leaderboard': leaderboard,'level_id':level_id})
    if level_id == 4:
        leaderboard = Level4.objects.order_by(order)
        return render(request, 'levels/leaderboard.html', {'leaderboard': leaderboard,'level_id':level_id})
    if level_id == 5:
        leaderboard = Level5.objects.order_by(order)
        return render(request, 'levels/leaderboard.html', {'leaderboard': leaderboard,'level_id':level_id})
