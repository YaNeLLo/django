from django.urls import path
from . import views

app_name = 'levels'
urlpatterns = [
    path('',views.index,name='index'),
    path('<int:level_id>/',views.level,name='level'),
    path('<int:level_id>/<str:sort_by>/',views.level,name='level'),
]