from django.db import models
from users.models import User

class Level1(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(verbose_name='Очки')
    coins = models.IntegerField(verbose_name='Монеты')
    time = models.IntegerField(verbose_name='Время')
    date = models.DateField(verbose_name='Дата',auto_now_add=True)

    def __str__(self):
        return self.user.login

    class Meta:
        verbose_name = '1 Уровень'
        verbose_name_plural = '1 Уровень'

class Level2(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(verbose_name='Очки')
    coins = models.IntegerField(verbose_name='Монеты')
    time = models.IntegerField(verbose_name='Время')
    date = models.DateField(verbose_name='Дата',auto_now_add=True)

    def __str__(self):
        return self.user.login

    class Meta:
        verbose_name = '2 Уровень'
        verbose_name_plural = '2 Уровень'

class Level3(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(verbose_name='Очки')
    coins = models.IntegerField(verbose_name='Монеты')
    time = models.IntegerField(verbose_name='Время')
    date = models.DateField(verbose_name='Дата',auto_now_add=True)

    def __str__(self):
        return self.user.login

    class Meta:
        verbose_name = '3 Уровень'
        verbose_name_plural = '3 Уровень'

class Level4(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(verbose_name='Очки')
    coins = models.IntegerField(verbose_name='Монеты')
    time = models.IntegerField(verbose_name='Время')
    date = models.DateField(verbose_name='Дата',auto_now_add=True)

    def __str__(self):
        return self.user.login

    class Meta:
        verbose_name = '4 Уровень'
        verbose_name_plural = '4 Уровень'

class Level5(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(verbose_name='Очки')
    coins = models.IntegerField(verbose_name='Монеты')
    time = models.IntegerField(verbose_name='Время')
    date = models.DateField(verbose_name='Дата',auto_now_add=True)

    def __str__(self):
        return self.user.login

    class Meta:
        verbose_name = '5 Уровень'
        verbose_name_plural = '5 Уровень'