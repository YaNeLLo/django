from django.contrib import admin
from .models import Level1,Level2,Level3,Level4,Level5

all = [Level1, Level2, Level3, Level4, Level5]

for i in all:
    @admin.register(i)
    class PersonAdmin(admin.ModelAdmin):
        list_display = ("user",'score','coins','time','date')
        list_filter = ('date', )

