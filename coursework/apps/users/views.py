from rest_framework import serializers
from users.models import User
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, NotFound, AuthenticationFailed
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['login','password']
        extra_kwargs = {
            'password':{'write_only':True},
        }
    
    def create(self,validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance
        
class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(methods=['POST'], detail=False, url_path='register')
    def register(self,request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'message':'success'})

    @action(methods=['POST'], detail=False, url_path='login')
    def login(self,request):
        if 'login' not in request.data:
            raise ValidationError({'error':'empty login'})
        if 'password' not in request.data:
            raise ValidationError({'error':'empty password'})

        try:
            user = User.objects.get(login=request.data['login'])
        except User.DoesNotExist:
            raise NotFound({'error':'no login in user list'})

        if not user.check_password(request.data['password']):
            raise AuthenticationFailed({'error':'wrong password'})

        refresh = RefreshToken.for_user(user)
        response = Response()
        response.data = {'access':str(refresh.access_token)}
        return response

    @action(methods=['GET'], detail=False, permission_classes=[IsAuthenticated],url_path='me')
    def get_user(self, request):
        user = request.user
        data = self.serializer_class(user).data
        return Response(data)