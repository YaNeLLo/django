from django.db import models

from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin

from users.managers import UserManager

class User(AbstractBaseUser,PermissionsMixin):
    login = models.CharField(verbose_name='Логин',max_length=255, unique=True)

    is_active = models.BooleanField(verbose_name='Активирован',default=True)
    is_staff = models.BooleanField(verbose_name='Модератор',default=False)
    is_superuser = models.BooleanField(verbose_name='Администратор',default=False)

    USERNAME_FIELD = 'login'
    # REQUIRED_FIELDS = ['login']

    objects = UserManager()

    def __str__(self):
        return self.login

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'